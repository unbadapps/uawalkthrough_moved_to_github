# UAWalkthrough

[![CI Status](http://img.shields.io/travis/marhas/UAWalkthrough.svg?style=flat)](https://travis-ci.org/marhas/UAWalkthrough)
[![Version](https://img.shields.io/cocoapods/v/UAWalkthrough.svg?style=flat)](http://cocoapods.org/pods/UAWalkthrough)
[![License](https://img.shields.io/cocoapods/l/UAWalkthrough.svg?style=flat)](http://cocoapods.org/pods/UAWalkthrough)
[![Platform](https://img.shields.io/cocoapods/p/UAWalkthrough.svg?style=flat)](http://cocoapods.org/pods/UAWalkthrough)

## Example

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Requirements

## Installation

UAWalkthrough is available through [CocoaPods](http://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
pod 'UAWalkthrough'
```

## Author

Marcel Hasselaar, marcel@hasselaar.nu

## License

UAWalkthrough is available under the MIT license. See the LICENSE file for more info.
